/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.sarocha.midtermexam;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

/**
 *
 * @author Sarocha
 */
public class ReverseNumbers {

    private static void Reverse(int[] num) {
        for (int i = 0; i < num.length / 2; i++) {
            int t = num[i];
            num[i] = num[num.length - i - 1];
            num[num.length - i - 1] = t;
        }
    }

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.print("Please input the number of numbers : ");
        int[] num = new int[kb.nextInt()];
        for (int i = 0; i < num.length; i++) {
            num[i] = kb.nextInt();
        }
        Reverse(num);
        System.out.printf("Reversed array is : " + Arrays.toString(num));
    }

}
